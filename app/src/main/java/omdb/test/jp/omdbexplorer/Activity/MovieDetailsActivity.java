package omdb.test.jp.omdbexplorer.Activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.Volley;

import org.w3c.dom.Text;

import java.io.IOException;

import omdb.test.jp.omdbexplorer.Model.Movie;
import omdb.test.jp.omdbexplorer.R;

/**
 * Created by JP on 02-01-2017.
 */

public class MovieDetailsActivity extends AppCompatActivity{

    private ImageView poster;
    private TextView year, title, imdbRating, plot, genre, actors, director;
    private ActionBar bar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_details);


        Intent intent = getIntent();
        Movie m = (Movie) intent.getExtras().getSerializable("movie");

        poster = (ImageView) findViewById(R.id.moviePoster);
        year = (TextView) findViewById(R.id.movieYear);
        plot = (TextView) findViewById(R.id.moviePlot);
        title = (TextView) findViewById(R.id.movieTitle);
        imdbRating = (TextView) findViewById(R.id.movieImdbRating);
        genre = (TextView) findViewById(R.id.movieGenre);
        actors = (TextView) findViewById(R.id.movieActors);
        director = (TextView) findViewById(R.id.movieDirector);
        if (m!=null){

            bar = getSupportActionBar();
            bar.setTitle(m.getTitle());
            bar.setDisplayHomeAsUpEnabled(true);

            try {
                getBitmapFromURL(m.getPoster(),poster);
            } catch (IOException e) {
                e.printStackTrace();
            }
            year.setText(String.valueOf(m.getYear()));
            plot.setText(m.getPlot());
            title.setText(m.getTitle());
            imdbRating.setText(m.getImdbRating());
            genre.setText(m.getGenre());
            actors.setText(m.getActors());
            director.setText(m.getDirector());
        }


    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                super.onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
    public void getBitmapFromURL(String src, final ImageView imgV) throws IOException {

        RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
        ImageRequest image = new ImageRequest(src,new Response.Listener<Bitmap>() {

            @Override
            public void onResponse(Bitmap response) {
                imgV.setImageBitmap(response);
            }
        }, 0, 0, null, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("Error: " + error.getMessage());

            }
        });
        queue.add(image);

    }

}
