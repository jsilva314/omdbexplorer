package omdb.test.jp.omdbexplorer.Util;

import org.json.JSONException;
import org.json.JSONObject;

import omdb.test.jp.omdbexplorer.Model.Movie;

/**
 * Created by JP on 30-12-2016.
 */

public class JSONParser {

    public Movie fromJSON(JSONObject json) throws JSONException {
        Movie m = new Movie();
            m.setYear(json.getString("Year"));
            m.setImbdID(json.getString("imdbID"));
            m.setPoster(json.getString("Poster"));
            m.setTitle(json.getString("Title"));
        return m;
    }
    public Movie fromJsonFullDetails(JSONObject json) throws JSONException {
        Movie m = new Movie();
            m.setTitle(json.getString("Title"));
            m.setYear(json.getString("Year"));
            m.setGenre(json.getString("Genre"));
            m.setDirector(json.getString("Director"));
            m.setActors(json.getString("Actors"));
            m.setPlot(json.getString("Plot"));
            m.setImdbRating(json.getString("imdbRating"));
            m.setPoster(json.getString("Poster"));
        return m;
    }
}
