package omdb.test.jp.omdbexplorer.Adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;


import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.Volley;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;

import omdb.test.jp.omdbexplorer.Model.Movie;
import omdb.test.jp.omdbexplorer.R;

/**
 * Created by JP on 30-12-2016.
 */

public class MovieListAdapter extends ArrayAdapter<Movie> {

    public MovieListAdapter(Context context, int textViewResourceId) {
        super(context, textViewResourceId);
    }

    public MovieListAdapter(Context context, int resource, ArrayList<Movie> items) {
        super(context, resource, items);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView;

        if (v == null) {
            LayoutInflater vi;
            vi = LayoutInflater.from(getContext());
            v = vi.inflate(R.layout.movie_list, null);
        }

        Movie p = getItem(position);

        if (p != null) {
            ImageView poster = (ImageView) v.findViewById(R.id.poster);
            TextView title = (TextView) v.findViewById(R.id.title);
            TextView year = (TextView) v.findViewById(R.id.year);

            if (poster != null) {
                try {
                    getBitmapFromURL(p.getPoster(),poster);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            if (title != null) {
                title.setText(p.getTitle());
            }

            if (year != null) {
                year.setText(String.valueOf(p.getYear()));
                Calendar calendar = Calendar.getInstance();
                int y = calendar.get(Calendar.YEAR);
                if (p.getYear().equals(String.valueOf(y))){
                    year.setTextColor(Color.RED);
                    year.setTypeface(null, Typeface.BOLD);
                }

            }
        }

        return v;
    }
    public void getBitmapFromURL(String src, final ImageView imgV) throws IOException {

        RequestQueue queue = Volley.newRequestQueue(getContext());
        ImageRequest image = new ImageRequest(src,new Response.Listener<Bitmap>() {

            @Override
            public void onResponse(Bitmap response) {
                imgV.setImageBitmap(response);
            }
        }, 0, 0, null, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                imgV.setImageBitmap(null);

            }
        });
        queue.add(image);

    }
}
