package omdb.test.jp.omdbexplorer.Activity;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import omdb.test.jp.omdbexplorer.Model.Movie;
import omdb.test.jp.omdbexplorer.Adapter.MovieListAdapter;
import omdb.test.jp.omdbexplorer.R;
import omdb.test.jp.omdbexplorer.Util.JSONParser;

public class MainActivity extends AppCompatActivity {

    private EditText searchText;
    private Button searchButton;
    private ListView moviesList;
    private ArrayList<Movie> movieArrayList = new ArrayList<>();
    private MovieListAdapter adapter;
    private Spinner spType, spResults;
    private Context context;
    private String main_url = "http://www.omdbapi.com/?";
    private JSONParser jParser = new JSONParser();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        context = getApplicationContext();

        searchText = (EditText) findViewById(R.id.searchEditText);
        searchButton = (Button) findViewById(R.id.searchButton);
        moviesList = (ListView) findViewById(R.id.moviesListView);

        spResults = (Spinner) findViewById(R.id.spResults);
        ArrayAdapter<CharSequence> resultsAdapter = ArrayAdapter.createFromResource(
                this, R.array.results, android.R.layout.simple_spinner_item);
        resultsAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spResults.setAdapter(resultsAdapter);

        spType = (Spinner) findViewById(R.id.spType);
        ArrayAdapter<CharSequence> typeAdapter = ArrayAdapter.createFromResource(
                this, R.array.type, android.R.layout.simple_spinner_item);
        typeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spType.setAdapter(typeAdapter);

        adapter = new MovieListAdapter(context, R.id.moviesListView, movieArrayList);
        moviesList.setAdapter(adapter);

        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!searchText.getText().toString().equals("")) {
                    movieArrayList.clear();
                    try {
                        searchMoviesByText(searchText.getText().toString());
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }

                }
            }
        });
        moviesList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(getBaseContext(), MovieDetailsActivity.class);
                intent.putExtra("movie", movieArrayList.get(position));
                startActivity(intent);
            }
        });
    }
    public void searchMoviesByText(String text) throws UnsupportedEncodingException {

        text = text.replace(" ", "%20");
        for (int page = 0; page < (Integer.parseInt(spResults.getSelectedItem().toString())) / 10; page++) {
            String url = main_url + "s=" + text + "&y=&type=" + spType.getSelectedItem().toString().toLowerCase() + "&page=" + page+1 + "&plot=short&r=json";
            RequestQueue queue = Volley.newRequestQueue(context);
            JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                    url, null,
                    new Response.Listener<JSONObject>() {

                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                JSONArray jsonArray = response.getJSONArray("Search");
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    Movie m = jParser.fromJSON(jsonArray.getJSONObject(i));
                                    searchMoviesByID(m.getImbdID());
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            } catch (UnsupportedEncodingException e) {
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    VolleyLog.d("Error: " + error.getMessage());

                }
            });
            queue.add(jsonObjReq);
        }
    }
    public void searchMoviesByID(String id) throws UnsupportedEncodingException {

        String url = main_url + "i=" + id + "&y=&plot=full&r=json";
        RequestQueue queue = Volley.newRequestQueue(context);
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,url, null,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            movieArrayList.add(jParser.fromJsonFullDetails(response));
                            adapter.notifyDataSetChanged();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("Error: " + error.getMessage());

            }
        });
        queue.add(jsonObjReq);
    }

}
